package config

import (
	"fmt"

	"github.com/gagliardetto/solana-go/rpc"
)

var (
	RpcEndpoint                  = rpc.MainNetBeta.RPC
	WsEndpoint                   = rpc.MainNetBeta.WS
	KHRONOS_PROGRAM_ID           = "" // Todo
	DEX_PROGRAM_ID               = "" // Todo
	LIQUIDITY_POOL_PROGRAM_V4_ID = "" // Todo
	TOKEN_PROGRAM                = "TokenkegQfeZyiNwAJbNbGKPFXCWuBvf9Ss623VQ5DA"
	SERUM_API_ENDPOINT           = "https://raw.githubusercontent.com/project-serum/serum-ts/master/packages/serum/src/"
)

func Init() {
	ParseFlags()

	if *FLAG_DEV {
		RpcEndpoint = rpc.DevNet.RPC
		WsEndpoint = rpc.DevNet.WS
		KHRONOS_PROGRAM_ID = "HSF658QaHvyK9khua3rv432Kv3WnY3vQGH1iSyjgUGof"
		DEX_PROGRAM_ID = "DESVgJVGajEgKGXhb6XmqDHGz3VjdgP7rEVESBgxmroY"
		LIQUIDITY_POOL_PROGRAM_V4_ID = "9rpQHSyFVM1dkkHFQ2TtTzPEW7DVmEyPmN8wVniqJtuC"
	} else if *FLAG_TEST {
		RpcEndpoint = rpc.TestNet.RPC
		WsEndpoint = rpc.TestNet.WS
	} else if *FLAG_LOCAL {
		RpcEndpoint = rpc.LocalNet_RPC
		WsEndpoint = rpc.LocalNet_WS
	}

	fmt.Println("Config:")
	fmt.Printf("Rpc Endpoint:\t\t %v\n", RpcEndpoint)
	fmt.Printf("Ws Endpoint:\t\t %v\n", WsEndpoint)
	fmt.Printf("Khronos program id:\t %v\n", KHRONOS_PROGRAM_ID)
	fmt.Printf("Serum program id:\t %v\n", DEX_PROGRAM_ID)
	fmt.Printf("Liquidity program id\t%v", LIQUIDITY_POOL_PROGRAM_V4_ID)

	fmt.Println("------------------------------------------------------------------------------")
}
