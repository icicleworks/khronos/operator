package config

import (
	"flag"
)

var FLAG_DEV *bool
var FLAG_TEST *bool
var FLAG_LOCAL *bool

func ParseFlags() {
	FLAG_DEV = flag.Bool("dev", false, "Use dev config")
	FLAG_TEST = flag.Bool("test", false, "Use test config")
	FLAG_LOCAL = flag.Bool("local", false, "Use local config")
	
	flag.Parse()
}
