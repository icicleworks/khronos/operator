package main

import (
	"context"

	solana "github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/rpc"
	"github.com/gagliardetto/solana-go/rpc/ws"
	"gitlab.com/icicleworks/khronos/cmd/config"
	"gitlab.com/icicleworks/khronos/internal/scheduler"
	"gitlab.com/icicleworks/khronos/internal/wallet"
)

var payer solana.PrivateKey
var rpcClient *rpc.Client
var wsClient *ws.Client

func main() {
	config.Init()

	payer = wallet.LoadWalletFromJson("wallet.json")
	rpcClient = rpc.New(config.RpcEndpoint)
	wsClient, _ = ws.Connect(context.Background(), config.WsEndpoint)

	scheduler.Start(rpcClient, wsClient, payer)
}
