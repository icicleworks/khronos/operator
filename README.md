# Khronos operator

Fetch Khronos vaults, filter by period and make swap on [Raydium DEX](https://raydium.io)

## Getting started
```
cd cmd
go run . -dev
```
