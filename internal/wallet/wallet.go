package wallet

import (
	"context"
	"fmt"
	"os"

	solana "github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/rpc"
	confirm "github.com/gagliardetto/solana-go/rpc/sendAndConfirmTransaction"
	"github.com/gagliardetto/solana-go/rpc/ws"
	"github.com/gagliardetto/solana-go/text"
)

func LoadWalletFromJson(path string) solana.PrivateKey {

	// Load private key from a json file generated with
	// $ solana-keygen new --outfile=standard.solana-keygen.json
	privateKey, err := solana.PrivateKeyFromSolanaKeygenFile(path)
	if err != nil {
		panic(err)
	}

	return privateKey
}

func SendAndConfirmTransaction(rpcClient *rpc.Client, wsClient *ws.Client, instructions []solana.Instruction, payer *solana.PrivateKey, signers []solana.PrivateKey) (*solana.Signature, error) {
	tx, err := createTransaction(
		rpcClient,
		instructions,
		payer,
		signers,
	)
	if err != nil {
		return nil, err
	}

	tx.EncodeTree(text.NewTreeEncoder(os.Stdout, "Signed trx"))

	sig, err := confirm.SendAndConfirmTransaction(
		context.TODO(),
		rpcClient,
		wsClient,
		tx,
	)
	if err != nil {
		return nil, err
	}

	return &sig, nil
}

func SimulateTransaction(rpcClient *rpc.Client, wsClient *ws.Client, instructions []solana.Instruction, payer *solana.PrivateKey, signers []solana.PrivateKey) error {
	tx, err := createTransaction(
		rpcClient,
		instructions,
		payer,
		signers,
	)
	if err != nil {
		return err
	}

	tx.EncodeTree(text.NewTreeEncoder(os.Stdout, "Signed trx"))

	sig, err := rpcClient.SimulateTransaction(
		context.TODO(),
		tx,
	)
	if err != nil {
		return err
	}
	if sig.Err != nil {
		return fmt.Errorf("Error simulating transaction: %+v", sig.Err)
	}
	return nil
}

func createTransaction(rpcClient *rpc.Client, instructions []solana.Instruction, payer *solana.PrivateKey, signers []solana.PrivateKey) (*solana.Transaction, error) {
	recent, err := rpcClient.GetRecentBlockhash(context.TODO(), rpc.CommitmentFinalized)
	if err != nil {
		return nil, err
	}

	tx, err := solana.NewTransaction(
		instructions,
		recent.Value.Blockhash,
		solana.TransactionPayer(payer.PublicKey()),
	)
	if err != nil {
		return nil, err
	}

	signers = append(signers, *payer)
	_, err = tx.Sign(
		func(key solana.PublicKey) *solana.PrivateKey {
			for i := 0; i < len(signers); i++ {
				if signers[i].PublicKey().Equals(key) {
					return &signers[i]
				}
			}
			return nil
		},
	)

	return tx, nil
}
