package raydium

import (
	"context"

	"github.com/gagliardetto/solana-go/rpc"

	"github.com/gagliardetto/solana-go"
	"gitlab.com/icicleworks/khronos/cmd/config"
)


func GetAmmByMarket(client *rpc.Client, marketAddress string) (amm *AmmInfoWrapper, err error) {
	ammId, _, err := solana.FindProgramAddress(
		[][]byte{
			solana.MustPublicKeyFromBase58(config.LIQUIDITY_POOL_PROGRAM_V4_ID).Bytes(),
			solana.MustPublicKeyFromBase58(marketAddress).Bytes(),
			[]byte(AMM_ASSOCIATED_SEED),
		},
		solana.MustPublicKeyFromBase58(config.LIQUIDITY_POOL_PROGRAM_V4_ID),
	)

	ctx := context.Background()

	ammInfoBytes, err := client.GetAccountInfo(ctx, ammId)
	if err != nil {
		return nil, err
	}

	ammInfo := &AmmInfo{}
	if err := ammInfo.Decode(ammInfoBytes.Value.Data.GetBinary()); err != nil {
		return nil, err
	}

	ammAuthority := GetAmmAuthority()

	return &AmmInfoWrapper{
		PublicKey: ammId,
		AmmInfo:   ammInfo,
		AmmAuthority: ammAuthority,
	}, nil
}

func GetAmmAuthority() solana.PublicKey {
	authority, _, _ := solana.FindProgramAddress(
		[][]byte{
			[]byte(AMM_AUTHORITY_SEED),
		},
		solana.MustPublicKeyFromBase58(config.LIQUIDITY_POOL_PROGRAM_V4_ID),
	)
	return authority
}
