package raydium

import (
	"fmt"

	bin "github.com/gagliardetto/binary"
	"github.com/gagliardetto/solana-go"
)

const AMM_ASSOCIATED_SEED = "amm_associated_seed"
const AMM_AUTHORITY_SEED = "amm authority"

type AmmInfoWrapper struct {
	PublicKey solana.PublicKey
	AmmInfo *AmmInfo
	AmmAuthority solana.PublicKey
}

type AmmInfo struct {
	/// 1 Initialized status.
	Status bin.Uint64
	/// Nonce used in program address.
	/// The program address is created deterministically with the nonce,
	/// amm program id, and amm account key.  This program address has
	/// authority over the amm's token coin account, token pc account, and pool
	/// token mint.
	Nonce bin.Uint64
	/// max order count
	Order_num bin.Uint64
	/// within this range, 5 => 5% range
	Depth bin.Uint64
	/// coin decimal
	Coin_decimals bin.Uint64
	/// pc decimal
	Pc_decimals bin.Uint64
	/// amm state
	State bin.Uint64
	/// amm reset_flag
	Reset_flag bin.Uint64
	/// min size 1->0.000001
	Min_size bin.Uint64
	/// vol_max_cut_ratio numerator, sys_decimal_value as denominator
	Vol_max_cut_ratio bin.Uint64
	/// amount wave numerator, sys_decimal_value as denominator
	Amount_wave bin.Uint64
	/// coinLotSize 1 -> 0.000001
	Coin_lot_size bin.Uint64
	/// pcLotSize 1 -> 0.000001
	Pc_lot_size bin.Uint64
	/// min_cur_price (2 * amm.order_num * amm.pc_lot_size) * max_price_multiplier
	Min_price_multiplier bin.Uint64
	/// max_cur_price (2 * amm.order_num * amm.pc_lot_size) * max_price_multiplier
	Max_price_multiplier bin.Uint64
	/// system decimal value, used to normalize the value of coin and pc amount
	Sys_decimal_value bin.Uint64
	/// All fee information
	Fees Fees
	/// data calc to output
	Out_put OutPutData
	/// Token coin
	Token_coin solana.PublicKey
	/// Token pc
	Token_pc solana.PublicKey
	/// Coin mint
	Coin_mint solana.PublicKey
	/// Pc mint
	Pc_mint solana.PublicKey
	/// lp mint
	Lp_mint solana.PublicKey
	/// open_orders key
	Open_orders solana.PublicKey
	/// market key
	Market solana.PublicKey
	/// serum dex key
	Serum_dex solana.PublicKey
	/// target_orders key
	Target_orders solana.PublicKey
	/// withdraw key
	Withdraw_queue solana.PublicKey
	/// temp lp key
	Token_temp_lp solana.PublicKey
	/// amm owner key
	Amm_owner solana.PublicKey
	/// pnl_owner key
	Pnl_owner solana.PublicKey
}

func (a *AmmInfo) Decode(in []byte) error {
	decoder := bin.NewBinDecoder(in)
	err := decoder.Decode(&a)
	if err != nil {
		return fmt.Errorf("unpack: %w", err)
	}
	return nil
}

type OutPutData struct {
	/// delay to take pnl coin
	Need_take_pnl_coin bin.Uint64
	/// delay to take pnl pc
	Need_take_pnl_pc bin.Uint64
	/// total pnl pc
	Total_pnl_pc bin.Uint64
	/// total pnl coin
	Total_pnl_coin bin.Uint64
	/// pool total deposit pc
	Pool_total_deposit_pc bin.Uint128
	/// pool total deposit coin
	Pool_total_deposit_coin bin.Uint128

	/// swap coin in amount
	Swap_coin_in_amount bin.Uint128
	/// swap pc out amount
	Swap_pc_out_amount bin.Uint128
	/// swap coin to pc fee
	Swap_coin2pc_fee bin.Uint64

	/// swap pc in amount
	Swap_pc_in_amount bin.Uint128
	/// swap coin out amount
	Swap_coin_out_amount bin.Uint128
	/// swap pc to coin fee
	Swap_pc2coin_fee bin.Uint64
}

type Fees struct {
	/// numerator of the min_separate
	Min_separate_numerator bin.Uint64
	/// denominator of the min_separate
	Min_separate_denominator bin.Uint64

	/// numerator of the fee
	Trade_fee_numerator bin.Uint64
	/// denominator of the fee
	/// and 'trade_fee_denominator' must be equal to 'min_separate_denominator'
	Trade_fee_denominator bin.Uint64

	/// numerator of the pnl
	Pnl_numerator bin.Uint64
	/// denominator of the pnl
	Pnl_denominator bin.Uint64

	/// numerator of the swap_fee
	Swap_fee_numerator bin.Uint64
	/// denominator of the swap_fee
	Swap_fee_denominator bin.Uint64
}
