package raydium

import (
	"testing"

	"github.com/gagliardetto/solana-go/rpc"
	"github.com/test-go/testify/assert"
	"gitlab.com/icicleworks/khronos/cmd/config"
)

var rpcClient = rpc.New(rpc.DevNet.RPC)

const marketAddress = "6RpePn7tRfuHBEtmGr99CacqnRXtJugNzUFpbxBeBYJ7"

func init() {
	config.LIQUIDITY_POOL_PROGRAM_V4_ID = "9rpQHSyFVM1dkkHFQ2TtTzPEW7DVmEyPmN8wVniqJtuC"
}

func TestGetAmmByMarket(t *testing.T) {
	wrapper, err := GetAmmByMarket(rpcClient, marketAddress)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, wrapper.AmmInfo.Market.String(), marketAddress)
	assert.Equal(t, wrapper.AmmInfo.Pc_mint.String(), "BaTgvQyjijS18rFQzwd69c8akQKnoZYZowU3PKFggaRP")
}
