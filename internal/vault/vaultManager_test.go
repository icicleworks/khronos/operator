package vault

import (
	"testing"

	"github.com/gagliardetto/solana-go/rpc"
	"github.com/test-go/testify/assert"
	"gitlab.com/icicleworks/khronos/cmd/config"
)

var rpcClient = rpc.New(rpc.DevNet.RPC)

func init() {
	config.KHRONOS_PROGRAM_ID = "HzbGTy3hVV7tdLTVTACudYruRdCf11xmaR82LZ62aNH8"
}

func TestGetActiveVaults(t *testing.T) {
	activeVaults := GetActiveVaults(rpcClient)
	allVaults := GetAllVaults(rpcClient)

	activeFromAllCount := 0
	for i := 0; i < len(allVaults); i++ {
		if allVaults[i].Vault.IsActive {
			activeFromAllCount++
		}
	}

	assert.Equal(t, len(activeVaults), activeFromAllCount)
}

func TestGetAllVaults(t *testing.T) {
	allVaults := GetAllVaults(rpcClient)

	assert.NotEqual(t, len(allVaults), 0)
}
