package vault

import (
	"context"
	"encoding/binary"
	"fmt"

	bin "github.com/gagliardetto/binary"
	"github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/programs/serum"
	"github.com/gagliardetto/solana-go/rpc"
	"gitlab.com/icicleworks/khronos/cmd/config"
	"gitlab.com/icicleworks/khronos/generated/khronos"
	"gitlab.com/icicleworks/khronos/internal/raydium"
)

func GetActiveVaults(client *rpc.Client) []VaultWrapper {
	filters := []rpc.RPCFilter{
		{
			Memcmp: &rpc.RPCFilterMemcmp{
				Offset: 116,
				Bytes:  []byte{1},
			},
		},
	}

	opts := &rpc.GetProgramAccountsOpts{
		Filters: filters,
	}

	out, err := client.GetProgramAccountsWithOpts(
		context.Background(),
		solana.MustPublicKeyFromBase58(config.KHRONOS_PROGRAM_ID),
		opts,
	)
	if err != nil {
		panic(err)
	}

	activeVaults := make([]VaultWrapper, 0)

	for i := 0; i < len(out); i++ {
		var vault khronos.Vault
		if bin.UnmarshalBin(&vault, out[i].Account.Data.GetBinary()) == nil {
			activeVaults = append(activeVaults, VaultWrapper{Vault: &vault, PublicKey: out[i].Pubkey})
		}
	}

	return activeVaults
}

func GetAllVaults(client *rpc.Client) []VaultWrapper {
	out, err := client.GetProgramAccounts(
		context.Background(),
		solana.MustPublicKeyFromBase58(config.KHRONOS_PROGRAM_ID),
	)
	if err != nil {
		panic(err)
	}

	activeVaults := make([]VaultWrapper, 0)

	for i := 0; i < len(out); i++ {
		var vault khronos.Vault
		err = bin.UnmarshalBin(&vault, out[i].Account.Data.GetBinary())
		if err == nil {
			activeVaults = append(activeVaults, VaultWrapper{Vault: &vault, PublicKey: out[i].Pubkey})
		}
	}

	return activeVaults
}

func GetSwapInstruction(vault *VaultWrapper, market *serum.MarketMeta, amm *raydium.AmmInfoWrapper) *khronos.Instruction {
	vault_deposit, _, _ := solana.FindProgramAddress(
		[][]byte{
			[]byte("deposit"),
			vault.PublicKey.ToPointer().Bytes(),
		}, solana.MustPublicKeyFromBase58(config.KHRONOS_PROGRAM_ID),
	)

	vault_destination, _, _ := solana.FindProgramAddress(
		[][]byte{
			[]byte("destination"),
			vault.PublicKey.ToPointer().Bytes(),
		}, solana.MustPublicKeyFromBase58(config.KHRONOS_PROGRAM_ID),
	)

	vault_authority, _, _ := solana.FindProgramAddress(
		[][]byte{[]byte("vault_seed")},
		solana.MustPublicKeyFromBase58(config.KHRONOS_PROGRAM_ID),
	)

	nonceBytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(nonceBytes, uint64(market.MarketV2.VaultSignerNonce))

	market_owner, _, _ := solana.FindProgramAddress(
		[][]byte{
			market.Address.ToPointer().Bytes(),
			nonceBytes,
		},
		solana.MustPublicKeyFromBase58(config.DEX_PROGRAM_ID),
	)

	swapTx := khronos.NewSwapInstruction(
		vault_deposit,
		vault_destination,
		vault_authority,
		vault.PublicKey,

		// Market
		amm.AmmInfo.Market,
		amm.PublicKey,
		amm.AmmAuthority,
		amm.AmmInfo.Open_orders,
		amm.AmmInfo.Target_orders,
		amm.AmmInfo.Token_coin,
		amm.AmmInfo.Token_pc,

		market.MarketV2.RequestQueue,
		market.MarketV2.EventQueue,
		market.MarketV2.Bids,
		market.MarketV2.Asks,
		market.MarketV2.BaseVault,
		market.MarketV2.QuoteVault,
		market_owner,

		solana.MustPublicKeyFromBase58(config.DEX_PROGRAM_ID),
		solana.MustPublicKeyFromBase58(config.LIQUIDITY_POOL_PROGRAM_V4_ID),
		solana.MustPublicKeyFromBase58(config.TOKEN_PROGRAM),
	)

	err := swapTx.Validate()
	if err != nil {
		fmt.Printf("Error validating: %v", err)
		return nil
	}

	return swapTx.Build()
}
