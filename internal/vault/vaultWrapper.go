package vault

import (
	"github.com/gagliardetto/solana-go"
	"gitlab.com/icicleworks/khronos/generated/khronos"
)

type VaultWrapper struct {
	Vault     *khronos.Vault
	PublicKey solana.PublicKey
}
