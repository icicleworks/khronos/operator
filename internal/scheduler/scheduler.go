package scheduler

import (
	"context"
	"time"

	"github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/rpc"
	"github.com/gagliardetto/solana-go/rpc/ws"
	"github.com/go-co-op/gocron"
	"gitlab.com/icicleworks/khronos/cmd/config"
	"gitlab.com/icicleworks/khronos/internal/operator"
)

var cron *gocron.Scheduler

func Start(rpcClient *rpc.Client, wsClient *ws.Client, payer solana.PrivateKey) {
	ctx := context.Background()
	cron = gocron.NewScheduler(time.UTC)

	startSwapScheduler(ctx, rpcClient, wsClient, payer, 1)

	cron.StartBlocking()
}

func startSwapScheduler(ctx context.Context, rpcClient *rpc.Client, wsClient *ws.Client, payer solana.PrivateKey, period int) {
	if *config.FLAG_DEV {
		cron.Every("1m").Do(operator.SwapTokens, ctx, rpcClient, wsClient, payer)
	} else {
		cron.Day().Do(operator.SwapTokens, ctx, rpcClient, wsClient, payer)
		// Todo add cron and do swaps by vault period
	}
}
