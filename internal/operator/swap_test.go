package operator

import (
	"context"
	"testing"

	"github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/rpc"
	"github.com/gagliardetto/solana-go/rpc/ws"
	"github.com/stretchr/testify/assert"
	"gitlab.com/icicleworks/khronos/cmd/config"
	"gitlab.com/icicleworks/khronos/internal/vault"
	"gitlab.com/icicleworks/khronos/internal/wallet"
)

var rpcClient = rpc.New(rpc.DevNet.RPC)
var wsClient *ws.Client
var payer solana.PrivateKey

func init() {
	config.KHRONOS_PROGRAM_ID = "HSF658QaHvyK9khua3rv432Kv3WnY3vQGH1iSyjgUGof"
	config.DEX_PROGRAM_ID = "DESVgJVGajEgKGXhb6XmqDHGz3VjdgP7rEVESBgxmroY"
	config.LIQUIDITY_POOL_PROGRAM_V4_ID = "9rpQHSyFVM1dkkHFQ2TtTzPEW7DVmEyPmN8wVniqJtuC"
	wsClient, _ = ws.Connect(context.Background(), rpc.DevNet.WS)
	payer = wallet.LoadWalletFromJson("../../cmd/wallet.json")
}

func TestSwapTokens(t *testing.T) {
	activeVaults := vault.GetActiveVaults(rpcClient)
	instructions := createInstructions(context.TODO(), rpcClient, activeVaults)

	signers := make([]solana.PrivateKey, 0)

	sig, err := wallet.SendAndConfirmTransaction(
		rpcClient,
		wsClient,
		instructions,
		&payer,
		signers,
	)

	t.Logf(": %v", sig)

	assert.Nil(t, err)
}
