package operator

import (
	"context"
	"fmt"

	"github.com/davecgh/go-spew/spew"
	"github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/rpc"
	"github.com/gagliardetto/solana-go/rpc/ws"
	"gitlab.com/icicleworks/khronos/internal/raydium"
	"gitlab.com/icicleworks/khronos/internal/serum"
	"gitlab.com/icicleworks/khronos/internal/vault"
	"gitlab.com/icicleworks/khronos/internal/wallet"
)

func SwapTokens(ctx context.Context, rpcClient *rpc.Client, wsClient *ws.Client, payer solana.PrivateKey) {
	activeVaults := vault.GetActiveVaults(rpcClient)
	instructions := createInstructions(ctx, rpcClient, activeVaults)

	signers := make([]solana.PrivateKey, 0)

	sig, err := wallet.SendAndConfirmTransaction(
		rpcClient,
		wsClient,
		instructions, 
		&payer,
		signers,
	)

	if err != nil {
		fmt.Printf("Error sending trx %v\n", err)
	} else {
		spew.Dump(sig)
	}
}

func createInstructions(ctx context.Context, rpcClient *rpc.Client, vaults []vault.VaultWrapper) []solana.Instruction {
	instructions := make([]solana.Instruction, 0)
	for i := 0; i < len(vaults); i++ {
		fmt.Printf("Getting swap instruction for: %+v\n", vaults[i])

		market := serum.GetMarketByPair(ctx, rpcClient, vaults[i].Vault.DepositMint.String(), vaults[i].Vault.DestinationMint.String())
		if market == nil {
			fmt.Println("Market not found!")
			break
		}

		ammInfo, err := raydium.GetAmmByMarket(rpcClient, market.MarketV2.OwnAddress.String())
		if err != nil {
			fmt.Printf("Amm for market %v not found!\n", market.Address.String())
			break
		}

		instruction := vault.GetSwapInstruction(&vaults[i], market, ammInfo)
		instructions = append(instructions, instruction)
	}
	return instructions
}
