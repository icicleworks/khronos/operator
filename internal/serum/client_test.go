package serum

import (
	"context"
	"testing"

	"github.com/gagliardetto/solana-go/rpc"
	"github.com/test-go/testify/assert"
	"gitlab.com/icicleworks/khronos/cmd/config"
)

var rpcClient *rpc.Client

func init() {
	rpcClient = rpc.New(rpc.DevNet_RPC)

	config.DEX_PROGRAM_ID = "DESVgJVGajEgKGXhb6XmqDHGz3VjdgP7rEVESBgxmroY"
}

func TestGetByPair(t *testing.T) {
	market := GetMarketByPair(context.TODO(), rpcClient, "LVtdcSgQcxRWqeMJhDTcjJGPBmfZYKTFu86ujUZevjW", "BaTgvQyjijS18rFQzwd69c8akQKnoZYZowU3PKFggaRP")

	assert.NotNil(t, market)
	assert.Equal(t, market.Address.String(), "6RpePn7tRfuHBEtmGr99CacqnRXtJugNzUFpbxBeBYJ7")

	market2 := GetMarketByPair(context.TODO(), rpcClient, "BaTgvQyjijS18rFQzwd69c8akQKnoZYZowU3PKFggaRP", "LVtdcSgQcxRWqeMJhDTcjJGPBmfZYKTFu86ujUZevjW")

	assert.NotNil(t, market2)
	assert.Equal(t, market2.Address.String(), "6RpePn7tRfuHBEtmGr99CacqnRXtJugNzUFpbxBeBYJ7")
}

func TestGetAllPairs(t *testing.T) {
	markets := GetAllPairs()

	assert.NotEqual(t, len(markets), 0)
}
