package serum

import (
	solana "github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/rpc"
)

func getByPairFilter(mint1, mint2 string) []rpc.RPCFilter {
	return []rpc.RPCFilter{
		{
			Memcmp: &rpc.RPCFilterMemcmp{
				Offset: 53,
				Bytes:  solana.MustPublicKeyFromBase58(mint1).Bytes(),
			},
		},
		{
			Memcmp: &rpc.RPCFilterMemcmp{
				Offset: 85,
				Bytes:  solana.MustPublicKeyFromBase58(mint2).Bytes(),
			},
		},
	}

}
