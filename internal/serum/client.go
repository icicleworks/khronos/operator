package serum

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"unsafe"

	solana "github.com/gagliardetto/solana-go"
	"github.com/gagliardetto/solana-go/programs/serum"
	"gitlab.com/icicleworks/khronos/cmd/config"

	systemProgram "github.com/gagliardetto/solana-go/programs/system"
	"github.com/gagliardetto/solana-go/rpc"
	"golang.org/x/net/context"
)

const OPEN_ORDERS_LENGTH = 3228

func CreateOrdersAccountInstruction(ctx context.Context, client *rpc.Client, owner solana.PublicKey, openOrderAccount solana.PublicKey) solana.Instruction {
	dataSize := uint64(OPEN_ORDERS_LENGTH)
	rent, _ := client.GetMinimumBalanceForRentExemption(ctx, dataSize, rpc.CommitmentConfirmed)

	tx := systemProgram.NewCreateAccountInstruction(
		rent,
		dataSize,
		solana.MustPublicKeyFromBase58(config.DEX_PROGRAM_ID),
		owner,
		openOrderAccount,
	)

	return tx.Build()
}

func GetAllPairs() []MarketInfo {
	response, err := http.Get(config.SERUM_API_ENDPOINT + "markets.json")
	if err != nil {
		log.Fatalln(err)
	}

	var allMarkets []MarketInfo
	if err := json.NewDecoder(response.Body).Decode(&allMarkets); err != nil {
		fmt.Println(err)
	}

	return getActiveMarkets(allMarkets)
}

func getActiveMarkets(markets []MarketInfo) []MarketInfo {
	activeMarkets := make([]MarketInfo, 0)
	for _, m := range markets {
		if !m.Deprecated {
			activeMarkets = append(activeMarkets, m)
		}
	}
	return activeMarkets
}

func GetMarketByPair(ctx context.Context, client *rpc.Client, mint1 string, mint2 string) *serum.MarketMeta {

	markets := getMarketByPairAcc(ctx, client, mint1, mint2)
	if markets == nil {
		markets = getMarketByPairAcc(ctx, client, mint2, mint1)
	}

	meta := &serum.MarketMeta{}

	market, err := parseMarketData(ctx, client, markets[0].Account.Data, meta)
	if err != nil {
		return nil
	}
	return market
}

func getMarketByPairAcc(ctx context.Context, client *rpc.Client, mint1 string, mint2 string) rpc.GetProgramAccountsResult {
	filters := getByPairFilter(mint1, mint2)

	opts := &rpc.GetProgramAccountsOpts{
		Filters: filters,
	}

	markets, err := client.GetProgramAccountsWithOpts(
		ctx,
		solana.MustPublicKeyFromBase58(config.DEX_PROGRAM_ID),
		opts,
	)

	if err != nil || len(markets) == 0 {
		return nil
	}
	return markets
}

func GetMarketByAddress(ctx context.Context, client *rpc.Client, address string) (*serum.MarketMeta, error) {
	marketAddr := solana.MustPublicKeyFromBase58(address)
	acctInfo, err := client.GetAccountInfo(ctx, marketAddr)
	if err != nil {
		return nil, fmt.Errorf("unable to get market account:%w", err)
	}

	meta := &serum.MarketMeta{}

	return parseMarketData(ctx, client, acctInfo.Value.Data, meta)
}

func parseMarketData(ctx context.Context, client *rpc.Client, acctInfo *rpc.DataBytesOrJSON, meta *serum.MarketMeta) (*serum.MarketMeta, error) {
	dataLen := len(acctInfo.GetBinary())

	switch dataLen {
	// case 380:
	// 	// if err := meta.MarketV1.Decode(acctInfo.Value.Data); err != nil {
	// 	// 	return nil, fmt.Errorf("decoding market v1: %w", err)
	// 	// }
	// 	return nil, fmt.Errorf("Unsupported market version, w/ data length of 380")

	case 388:
		if err := meta.MarketV2.Decode(acctInfo.GetBinary()); err != nil {
			return nil, fmt.Errorf("decoding market v2: %w", err)
		}

	default:
		return nil, fmt.Errorf("unsupported market data length: %d", dataLen)
	}

	if err := client.GetAccountDataInto(ctx, meta.MarketV2.QuoteMint, &meta.QuoteMint); err != nil {
		return nil, fmt.Errorf("getting quote mint: %w", err)
	}

	if err := client.GetAccountDataInto(ctx, meta.MarketV2.BaseMint, &meta.BaseMint); err != nil {
		return nil, fmt.Errorf("getting base token: %w", err)
	}

	meta.Address = meta.MarketV2.OwnAddress

	return meta, nil
}

func getOpenOrdersSize() uintptr {
	var obj *serum.OpenOrders
	s := unsafe.Sizeof(*obj)
	return s
}
