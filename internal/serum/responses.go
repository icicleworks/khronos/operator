package serum

type MarketInfo struct {
	Address    string `json:"address"`
	Deprecated bool   `json:"deprecated"`
	Name       string `json:"name"`
	ProgramID  string `json:"programId"`
}